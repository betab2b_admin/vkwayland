/*
 * Copyright (C) 2018 by Michael Mestnik
 * This code is licensed under the MIT license (MIT)
 * (http://opensource.org/licenses/MIT)
 */
#ifndef VULKAN_EXTENDED_H_
#define VULKAN_EXTENDED_H_ 1

#ifdef __cplusplus
extern "C"
{
#endif

#ifndef VK_EXT_image_drm_format_modifier

        enum _VkResult
        {
                VK_ERROR_INVALID_DRM_FORMAT_MODIFIER_PLANE_LAYOUT_EXT = -1000158000,
        };

        enum _VkStructureType
        {
                VK_STRUCTURE_TYPE_DRM_FORMAT_MODIFIER_PROPERTIES_LIST_EXT = 1000158000,
                VK_STRUCTURE_TYPE_DRM_FORMAT_MODIFIER_PROPERTIES_EXT = 1000158001,
                VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_IMAGE_DRM_FORMAT_MODIFIER_INFO_EXT = 1000158002,
                VK_STRUCTURE_TYPE_IMAGE_DRM_FORMAT_MODIFIER_LIST_CREATE_INFO_EXT = 1000158003,
                VK_STRUCTURE_TYPE_IMAGE_EXCPLICIT_DRM_FORMAT_MODIFIER_CREATE_INFO_EXT = 1000158004,
                VK_STRUCTURE_TYPE_IMAGE_DRM_FORMAT_MODIFIER_PROPERTIES_EXT = 1000158005,
        };

        enum _VkImageTiling
        {
                VK_IMAGE_TILING_DRM_FORMAT_MODIFIER_EXT = 1000158000,
        };

        enum _VkImageAspectFlagBits
        {
                VK_IMAGE_ASPECT_MEMORY_PLANE_0_BIT_EXT = 0x00000080,
                VK_IMAGE_ASPECT_MEMORY_PLANE_1_BIT_EXT = 0x00000100,
                VK_IMAGE_ASPECT_MEMORY_PLANE_2_BIT_EXT = 0x00000200,
                VK_IMAGE_ASPECT_MEMORY_PLANE_3_BIT_EXT = 0x00000400,
        };

#define VK_EXT_image_drm_format_modifier 1
#define VK_EXT_EXTENSION_159_SPEC_VERSION 0
#define VK_EXT_EXTENSION_159_EXTENSION_NAME "VK_EXT_extension_159"
#define VK_EXT_IMAGE_DRM_FORMAT_MODIFIER_SPEC_VERSION 1
#define VK_EXT_IMAGE_DRM_FORMAT_MODIFIER_EXTENSION_NAME "VK_EXT_image_drm_format_modifier"

        typedef struct VkDrmFormatModifierPropertiesEXT
        {
                uint64_t drmFormatModifier;
                uint32_t drmFormatModifierPlaneCount;
                VkFormatFeatureFlags drmFormatModifierTilingFeatures;
        } VkDrmFormatModifierPropertiesEXT;

        typedef struct VkDrmFormatModifierPropertiesListEXT
        {
                VkStructureType sType;
                void *pNext;
                uint32_t drmFormatModifierCount;
                VkDrmFormatModifierPropertiesEXT *pDrmFormatModifierProperties;
        } VkDrmFormatModifierPropertiesListEXT;

        typedef struct VkPhysicalDeviceImageDrmFormatModifierInfoEXT
        {
                VkStructureType sType;
                const void *pNext;
                uint64_t drmFormatModifier;
        } VkPhysicalDeviceImageDrmFormatModifierInfoEXT;

        typedef struct VkImageDrmFormatModifierListCreateInfoEXT
        {
                VkStructureType sType;
                const void *pNext;
                uint32_t drmFormatModifierCount;
                const uint64_t *pDrmFormatModifiers;
        } VkImageDrmFormatModifierListCreateInfoEXT;

        typedef struct VkImageDrmFormatModifierExplicitCreateInfoEXT
        {
                VkStructureType sType;
                const void *pNext;
                uint64_t drmFormatModifier;
                uint32_t drmFormatModifierPlaneCount;
                const VkSubresourceLayout *pPlaneLayouts;
        } VkImageDrmFormatModifierExplicitCreateInfoEXT;

        typedef struct VkImageDrmFormatModifierPropertiesEXT
        {
                VkStructureType sType;
                void *pNext;
                uint64_t drmFormatModifier;
        } VkImageDrmFormatModifierPropertiesEXT;

        typedef VkResult(VKAPI_PTR *PFN_vkGetImageDrmFormatModifierPropertiesEXT)(VkDevice device, VkImage image, VkImageDrmFormatModifierPropertiesEXT *pProperties);

#ifndef VK_NO_PROTOTYPES
        VKAPI_ATTR VkResult VKAPI_CALL vkGetImageDrmFormatModifierPropertiesEXT(
            VkDevice device,
            VkImage image,
            VkImageDrmFormatModifierPropertiesEXT *pProperties);
#endif

#endif

#ifdef __cplusplus
}
#endif

#endif
