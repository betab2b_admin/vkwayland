/*
 * Copyright (C) 2018 by Michael Mestnik
 * This code is licensed under the MIT license (MIT)
 * (http://opensource.org/licenses/MIT)
 */
#include "wayland.hpp"

namespace vkc
{

namespace
{

void get_pointer(struct wl_client *client,
                 struct wl_resource *resource,
                 uint32_t id)
{
        return;
}

void get_keyboard(struct wl_client *client,
                  struct wl_resource *resource,
                  uint32_t id)
{
        return;
}

void get_touch(struct wl_client *client,
               struct wl_resource *resource,
               uint32_t id)
{
        return;
}

void release(struct wl_client *client,
             struct wl_resource *resource)
{
        return;
}

static const struct wl_seat_interface seat_interface = {get_pointer, get_keyboard, get_touch, release};

static void
bind(struct wl_client *client, void *data,
     uint32_t version, uint32_t id)
{
        // struct weston_compositor *compositor = data;
        struct wl_resource *resource;

        resource = wl_resource_create(client, &wl_seat_interface,
                                      version, id);
        if (resource == NULL)
        {
                wl_client_post_no_memory(client);
                return;
        }

        wl_resource_set_implementation(resource, &seat_interface, data, nullptr);

        wl_seat_send_capabilities(resource, WL_SEAT_CAPABILITY_POINTER | WL_SEAT_CAPABILITY_KEYBOARD);
        wl_seat_send_name(resource, "seat12");
}

} // namespace

bool Wayland::Seat::Init()
{
        return singleton_wayland->GlobalCreate(&wl_seat_interface, 1, nullptr, &bind);
}

} // namespace vkc
