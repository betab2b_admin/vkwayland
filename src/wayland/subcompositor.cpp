/*
 * Copyright (C) 2018 by Michael Mestnik
 * This code is licensed under the MIT license (MIT)
 * (http://opensource.org/licenses/MIT)
 */
#include "wayland.hpp"

namespace vkc
{

namespace
{

static void
bind(struct wl_client *client, void *data,
     uint32_t version, uint32_t id)
{
        return;
}

} // namespace

bool Wayland::Subcompositor::Init()
{
        return singleton_wayland->GlobalCreate(&wl_subcompositor_interface, 1, nullptr, &bind);
}

} // namespace vkc
