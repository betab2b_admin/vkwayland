/*
 * Copyright (C) 2018 by Michael Mestnik
 * This code is licensed under the MIT license (MIT)
 * (http://opensource.org/licenses/MIT)
 */
#include "wayland.hpp"

#include "wayland-xdg-shell-unstable-v6-server-protocol.h"

namespace vkc
{

namespace
{

void destroy(struct wl_client *client,
             struct wl_resource *resource)
{
        return;
}

void create_positioner(struct wl_client *client,
                       struct wl_resource *resource,
                       uint32_t id)
{
        return;
}

void surface_destroy(struct wl_client *client,
                     struct wl_resource *resource)
{
        return;
}

void toplevel_destroy(struct wl_client *client,
                      struct wl_resource *resource)
{
        return;
}

void toplevel_set_parent(struct wl_client *client,
                         struct wl_resource *resource,
                         struct wl_resource *parent)
{
        return;
}

void toplevel_set_title(struct wl_client *client,
                        struct wl_resource *resource,
                        const char *title)
{
        return;
}

void toplevel_set_app_id(struct wl_client *client,
                         struct wl_resource *resource,
                         const char *app_id)
{
        return;
}

void toplevel_show_window_menu(struct wl_client *client,
                               struct wl_resource *resource,
                               struct wl_resource *seat,
                               uint32_t serial,
                               int32_t x,
                               int32_t y)
{
        return;
}

void toplevel_move(struct wl_client *client,
                   struct wl_resource *resource,
                   struct wl_resource *seat,
                   uint32_t serial)
{
        return;
}

void toplevel_resize(struct wl_client *client,
                     struct wl_resource *resource,
                     struct wl_resource *seat,
                     uint32_t serial,
                     uint32_t edges)
{
        return;
}

void toplevel_set_max_size(struct wl_client *client,
                           struct wl_resource *resource,
                           int32_t width,
                           int32_t height)
{
        return;
}

void toplevel_set_min_size(struct wl_client *client,
                           struct wl_resource *resource,
                           int32_t width,
                           int32_t height)
{
        return;
}

void toplevel_set_maximized(struct wl_client *client,
                            struct wl_resource *resource)
{
        return;
}

void toplevel_unset_maximized(struct wl_client *client,
                              struct wl_resource *resource)
{
        return;
}

void toplevel_set_fullscreen(struct wl_client *client,
                             struct wl_resource *resource,
                             struct wl_resource *output)
{
        return;
}

void toplevel_unset_fullscreen(struct wl_client *client,
                               struct wl_resource *resource)
{
        return;
}

void toplevel_set_minimized(struct wl_client *client,
                            struct wl_resource *resource)
{
        return;
}

static const struct zxdg_toplevel_v6_interface zxdg_toplevel = {
    toplevel_destroy,
    toplevel_set_parent,
    toplevel_set_title,
    toplevel_set_app_id,
    toplevel_show_window_menu,
    toplevel_move,
    toplevel_resize,
    toplevel_set_max_size,
    toplevel_set_min_size,
    toplevel_set_maximized,
    toplevel_unset_maximized,
    toplevel_set_fullscreen,
    toplevel_unset_fullscreen,
    toplevel_set_minimized};

void surface_get_toplevel(struct wl_client *wl_client,
                          struct wl_resource *resource,
                          uint32_t id)
{
        Wayland::zxdg_shell_v6::Surface *dsurface =
            static_cast<Wayland::zxdg_shell_v6::Surface *>(wl_resource_get_user_data(resource));

        Wayland::zxdg_shell_v6::Surface::TopLevel *toplevel = new Wayland::zxdg_shell_v6::Surface::TopLevel();

#if 0
	toplevel->resource =
		weston_desktop_surface_add_resource(toplevel->base.desktop_surface,
						    &zxdg_toplevel_v6_interface,
						    &weston_desktop_xdg_toplevel_implementation,
						    id, weston_desktop_xdg_toplevel_resource_destroy);
	if (toplevel->resource == NULL)
		return;

	toplevel->base.role = WESTON_DESKTOP_XDG_SURFACE_ROLE_TOPLEVEL;
#else
        toplevel->resource = wl_resource_create(wl_client, &zxdg_toplevel_v6_interface, 1, id);
        if (toplevel->resource == NULL)
        {
                wl_client_post_no_memory(wl_client);
                return;
        }

        wl_resource_set_implementation(toplevel->resource, &zxdg_toplevel, toplevel, [](wl_resource *resource) { delete static_cast<Wayland::zxdg_shell_v6::Surface::TopLevel *>(wl_resource_get_user_data(resource)); });

        wl_array states;
        wl_array_init(&states);
        zxdg_toplevel_v6_send_configure(toplevel->resource, 0, 0, &states);
#endif
}

void surface_get_popup(struct wl_client *client,
                       struct wl_resource *resource,
                       uint32_t id,
                       struct wl_resource *parent,
                       struct wl_resource *positioner)
{
        return;
}

void surface_set_window_geometry(struct wl_client *client,
                                 struct wl_resource *resource,
                                 int32_t x,
                                 int32_t y,
                                 int32_t width,
                                 int32_t height)
{
        return;
}

void surface_ack_configure(struct wl_client *client,
                           struct wl_resource *resource,
                           uint32_t serial)
{
        return;
}

static const struct zxdg_surface_v6_interface zxdg_surface = {surface_destroy, surface_get_toplevel, surface_get_popup, surface_set_window_geometry, surface_ack_configure};

static void
get_xdg_surface(struct wl_client *wl_client,
                struct wl_resource *resource,
                uint32_t id,
                struct wl_resource *surface_resource)
{
        void *data =
            wl_resource_get_user_data(resource);
        Wayland_Compositor_Surface *wsurface =
            static_cast<Wayland_Compositor_Surface *>(wl_resource_get_user_data(surface_resource));
        Wayland::zxdg_shell_v6::Surface *surface = new Wayland::zxdg_shell_v6::Surface();

        if (surface == NULL)
        {
                wl_client_post_no_memory(wl_client);
                return;
        }

        // surface->desktop = weston_desktop_client_get_desktop(client);
        surface->surface = wsurface;

#if 0
        surface->desktop_surface =
            weston_desktop_surface_create(surface->desktop, client,
                                          surface->surface,
                                          &weston_desktop_xdg_surface_internal_implementation,
                                          surface);
        if (surface->desktop_surface == NULL)
        {
                free(surface);
                return;
        }

        surface->resource =
            weston_desktop_surface_add_resource(surface->desktop_surface,
                                                &zxdg_surface_v6_interface,
                                                &weston_desktop_xdg_surface_implementation,
                                                id, weston_desktop_xdg_surface_resource_destroy);
        if (surface->resource == NULL)
                return;

        if (wsurface->buffer_ref.buffer != NULL)
        {
                wl_resource_post_error(surface->resource,
                                       ZXDG_SURFACE_V6_ERROR_UNCONFIGURED_BUFFER,
                                       "xdg_surface must not have a buffer at creation");
                return;
        }
#else
        surface->resource = wl_resource_create(wl_client, &zxdg_surface_v6_interface, 1, id);
        if (surface->resource == NULL)
        {
                wl_client_post_no_memory(wl_client);
                return;
        }

        wl_resource_set_implementation(surface->resource, &zxdg_surface, surface, [](wl_resource *resource) { delete static_cast<Wayland::zxdg_shell_v6::Surface *>(wl_resource_get_user_data(resource)); });

        zxdg_surface_v6_send_configure(surface->resource, 6);
#endif
}

void pong(struct wl_client *client,
          struct wl_resource *resource,
          uint32_t serial)
{
        return;
}

static const struct zxdg_shell_v6_interface zxdg_shell = {destroy, create_positioner, get_xdg_surface, pong};

static void
bind(struct wl_client *client, void *data, uint32_t version, uint32_t id)
{
        struct wl_resource *resource;

        resource = wl_resource_create(client, &zxdg_shell_v6_interface,
                                      version, id);
        if (resource == NULL)
        {
                wl_client_post_no_memory(client);
                return;
        }

        wl_resource_set_implementation(resource, &zxdg_shell, data, nullptr);
}

} // namespace

bool Wayland::zxdg_shell_v6::Init()
{
        return singleton_wayland->GlobalCreate(&zxdg_shell_v6_interface, 1, nullptr, &bind);
}

} // namespace vkc
