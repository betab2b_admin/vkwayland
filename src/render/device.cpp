/*
 * Copyright (C) 2018 by Ilya Glushchenko
 * Copyright (C) 2018 by Michael Mestnik
 * This code is licensed under the MIT license (MIT)
 * (http://opensource.org/licenses/MIT)
 */
#include <vulkanio.hpp>
#include "outsurface.hpp"
#include "surface.hpp"
#include "structs.hpp"
#include <iostream>
#include <unistd.h>

#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>

namespace vkc
{

bool Render::Device::Init(vk::PhysicalDevice p, vk::Device d, uint32_t familyIndex, std::set<std::string> device_extentions)
{
        m_physical = p;
        m_logical = d;
        m_familyIndex = familyIndex;
        m_queue = m_logical.getQueue(familyIndex, 0);
        m_device_extentions = device_extentions;
        return CreateSemaphore() &&
               CreateFence() &&
               CreateSampler() &&
               CreateShaders() &&
               CreateVMA() &&
               CreateDescriptorPool() &&
               CreateDescriptorLayout() &&
               CreatePipeline() &&
               CreateVertexBuffer() &&
               CreateCommandPool() &&
               CreatePrimaryBuffer() &&
               true;
}

void Render::Device::Shutdown()
{
        // m_logical.waitIdle();
        m_outstandingBuffers.clear();

        for (auto it = m_buffers.cbegin(); it != m_buffers.cend();)
        {
                delete it->second;
                m_buffers.erase(it++);
        }

        for (auto it = m_surfaces.cbegin(); it != m_surfaces.cend();)
        {
                delete it->second;
                m_surfaces.erase(it++);
        }

        if (m_commandBuffer)
                m_logical.freeCommandBuffers(m_commandPool, m_commandBuffer);
        if (m_commandPool)
                m_logical.destroyCommandPool(m_commandPool);
        if (m_vertexBuffer->buffer && m_vertexBuffer->allocation)
                vmaDestroyBuffer(m_allocator, m_vertexBuffer->buffer, m_vertexBuffer->allocation);
        if (m_vertexShader)
                m_logical.destroyShaderModule(m_vertexShader->shaderModule);
        if (m_sampler)
                m_logical.destroySampler(m_sampler);
        if (m_fragmentShader)
                m_logical.destroyShaderModule(m_fragmentShader->shaderModule);
        if (m_pipelineCache)
                m_logical.destroyPipelineCache(m_pipelineCache);
        if (m_pipelineLayout)
                m_logical.destroyPipelineLayout(m_pipelineLayout);
        if (m_descriptorLayouts[1])
                m_logical.destroyDescriptorSetLayout(m_descriptorLayouts[1]);
        if (m_descriptorLayouts[0])
                m_logical.destroyDescriptorSetLayout(m_descriptorLayouts[0]);
        if (m_descriptorPool)
                m_logical.destroyDescriptorPool(m_descriptorPool);
        if (m_allocator)
                vmaDestroyAllocator(m_allocator);
        if (m_fence)
                m_logical.destroyFence(m_fence);
        if (m_renderDoneSemaphore)
                m_logical.destroySemaphore(m_renderDoneSemaphore);
}

bool Render::Device::InitDMABUF()
{
        vk::DispatchLoaderDynamic dldi(singleton_instance);

        vk::PhysicalDeviceExternalBufferInfo externalBufferInfo;
        externalBufferInfo.setUsage(vk::BufferUsageFlagBits::eTransferSrc);
        externalBufferInfo.setHandleType(vk::ExternalMemoryHandleTypeFlagBits::eDmaBufEXT);

        vk::ExternalBufferProperties externalBufferProperties;
        externalBufferProperties = m_physical.getExternalBufferProperties(externalBufferInfo, dldi);
        if (externalBufferProperties.externalMemoryProperties.externalMemoryFeatures == vk::ExternalMemoryFeatureFlagBits::eImportable)
        {
                std::cerr << "Warning: Can't import DMABUF" << std::endl;
                return false;
        }

        return true;
}

bool Render::Device::InsertSurface(vk::SurfaceKHR surface)
{
        vk::Extent2D imageSize;
        if (GetSurfaceSupportKHR(surface) && CreateSurfaceSwapchain(surface, &imageSize) && CreateSurfaceSemaphore())
        {
                m_surfaces.insert(std::make_pair(surface, new OutSurface()));
                if (m_surfaces[surface]->Init(
                        this,
                        surface,
                        m_swapchains.back(),
                        imageSize,
                        m_imageAvailableSemaphores.back()))
                        return true;
                delete m_surfaces[surface];
                m_surfaces.erase(surface);
        }
        return false;
}

void Render::Device::RemoveSurface(vk::SurfaceKHR surface)
{
        delete m_surfaces[surface];
        m_imageAvailableSemaphores.erase(std::remove(m_imageAvailableSemaphores.begin(), m_imageAvailableSemaphores.end(), m_surfaces[surface]->m_imageAvailableSemaphore), m_imageAvailableSemaphores.end());
        m_swapchains.erase(std::remove(m_swapchains.begin(), m_swapchains.end(), m_surfaces[surface]->m_swapchain), m_swapchains.end());
        m_surfaces.erase(surface);
}

const static vk::ImageSubresourceRange subresourceRange(vk::ImageAspectFlagBits::eColor, 0, 1, 0, 1);

bool Render::Device::Frame()
{
        status = m_logical.waitIdle();
        if (status != vk::Result::eSuccess)
        {
                std::cerr << "Failed to go idle." << std::endl;
                return false;
        }

        vk::CommandBufferBeginInfo beginInfo;
        beginInfo.setFlags(vk::CommandBufferUsageFlagBits::eOneTimeSubmit);
        m_commandBuffer.begin(beginInfo);

        if (!m_outstandingBuffers.empty())
        {
                std::vector<vk::ImageMemoryBarrier> barriers;
                {
                        vk::ImageMemoryBarrier barrier;
                        barrier.setOldLayout(vk::ImageLayout::eUndefined);
                        barrier.setNewLayout(vk::ImageLayout::eTransferDstOptimal);
                        barrier.setDstAccessMask(vk::AccessFlagBits::eTransferWrite);
                        barrier.setSubresourceRange(subresourceRange);

                        for (auto it : m_outstandingBuffers)
                        {
                                if (!it->m_buffer)
                                        continue;
                                barrier.setImage(it->m_textureImage);
                                barriers.insert(barriers.end(), barrier);
                        }
                }
                {
                        m_commandBuffer.pipelineBarrier(
                            vk::PipelineStageFlagBits::eTopOfPipe,
                            vk::PipelineStageFlagBits::eTransfer,
                            vk::DependencyFlags(0),
                            nullptr, nullptr, barriers);

                        for (auto it : m_outstandingBuffers)
                        {
                                if (!it->m_buffer)
                                        continue;
                                m_commandBuffer.copyBufferToImage(
                                    it->m_buffer,
                                    it->m_textureImage,
                                    vk::ImageLayout::eTransferDstOptimal,
                                    it->m_regions);
                                it->m_regions.clear();
                        }
                }
                {
                        vk::ImageMemoryBarrier barrier;
                        barrier.setOldLayout(vk::ImageLayout::eTransferDstOptimal);
                        barrier.setNewLayout(vk::ImageLayout::eShaderReadOnlyOptimal);
                        barrier.setSrcAccessMask(vk::AccessFlagBits::eTransferWrite);
                        barrier.setDstAccessMask(vk::AccessFlagBits::eShaderRead);

                        barriers = {};
                        for (auto it : m_outstandingBuffers)
                        {
                                if (!it->m_buffer)
                                        continue;
                                barrier.setImage(it->m_textureImage);
                                barriers.insert(barriers.end(), barrier);
                        }
                        m_outstandingBuffers = {};
                }
                {
                        m_commandBuffer.pipelineBarrier(
                            vk::PipelineStageFlagBits::eTransfer,
                            vk::PipelineStageFlagBits::eFragmentShader,
                            vk::DependencyFlags(0),
                            nullptr, nullptr, barriers);
                }
        }

        bool ret = true;
        for (auto it : m_surfaces)
                ret = it.second->Frame() && ret;

        m_commandBuffer.end();

        vk::SubmitInfo submitInfo;
        submitInfo.setCommandBufferCount(1);
        submitInfo.setPCommandBuffers(&m_commandBuffer);
        vk::PipelineStageFlags waitStage = vk::PipelineStageFlagBits::eColorAttachmentOutput;
        submitInfo.setPWaitDstStageMask(&waitStage);
        submitInfo.setWaitSemaphoreCount(static_cast<uint32_t>(m_imageAvailableSemaphores.size()));
        submitInfo.setPWaitSemaphores(m_imageAvailableSemaphores.data());
        submitInfo.setSignalSemaphoreCount(1);
        submitInfo.setPSignalSemaphores(&m_renderDoneSemaphore);

        status = m_queue.submit(1, &submitInfo, m_fence);
        if (status != vk::Result::eSuccess)
        {
                std::cerr << "Failed to submit cmd." << std::endl;
                return false;
        }

        std::vector<vk::SwapchainKHR> swapchains;
        std::vector<uint32_t> framebuffers;

        for (auto it : m_surfaces)
        {
                swapchains.push_back(it.second->m_swapchain);
                framebuffers.push_back(it.second->m_currentFrameBuffer);
        }

        while (m_logical.waitForFences(vk::ArrayProxy<const vk::Fence>(m_fence), true, UINT64_MAX) == vk::Result::eTimeout)
                ;
        m_logical.resetFences(vk::ArrayProxy<const vk::Fence>(m_fence));

        vk::PresentInfoKHR presentInfo;
        presentInfo.setWaitSemaphoreCount(1);
        presentInfo.setPWaitSemaphores(&m_renderDoneSemaphore);
        presentInfo.setSwapchainCount(swapchains.size());
        presentInfo.setPSwapchains(swapchains.data());
        presentInfo.setPImageIndices(framebuffers.data());
        status = m_queue.presentKHR(presentInfo);
        if (status != vk::Result::eSuccess)
        {
                std::cerr << "Failed to present queue." << std::endl;
                return false;
        }

        return ret;
}

bool Render::Device::GetSurfaceSupportKHR(vk::SurfaceKHR surface)
{
        vk::Bool32 supported;
        std::tie(status, supported) = m_physical.getSurfaceSupportKHR(m_familyIndex, surface);
        if (status != vk::Result::eSuccess)
        {
                std::cerr << "Failed to get surface support." << std::endl;
                return false;
        }
        return supported;
}

bool Render::Device::CreateSemaphore()
{
        vk::SemaphoreCreateInfo createInfo;
        std::tie(status, m_renderDoneSemaphore) = m_logical.createSemaphore(createInfo);
        if (status != vk::Result::eSuccess)
        {
                std::cerr << "Failed to create render done semaphore." << std::endl;
                return false;
        }
        return true;
}

bool Render::Device::CreateFence()
{
        vk::FenceCreateInfo createInfo;
        std::tie(status, m_fence) = m_logical.createFence(createInfo);
        if (status != vk::Result::eSuccess)
        {
                std::cerr << "Failed to create fence." << std::endl;
                return false;
        }
        return true;
}

bool Render::Device::CreateSurfaceSwapchain(vk::SurfaceKHR surface, vk::Extent2D *imageSize)
{
        vk::SurfaceCapabilitiesKHR capabilities;
        std::tie(status, capabilities) = m_physical.getSurfaceCapabilitiesKHR(surface);
        if (status != vk::Result::eSuccess)
        {
                std::cerr << "Failed to get surface capabilities." << std::endl;
                return false;
        }

        if (!(capabilities.supportedCompositeAlpha & vk::CompositeAlphaFlagBitsKHR::eOpaque))
        {
                std::cerr << "Surface does not support opaque composite alpha." << std::endl;
                status = vk::Result::eErrorInitializationFailed;
                return false;
        }

        if (capabilities.maxImageCount != 0 && capabilities.maxImageCount < 2)
        {
                std::cerr << "Surface does not support double buffering." << std::endl;
                status = vk::Result::eErrorInitializationFailed;
                return false;
        }

        std::vector<vk::SurfaceFormatKHR> imageFormats;
        std::tie(status, imageFormats) = m_physical.getSurfaceFormatsKHR(surface);
        if (status != vk::Result::eSuccess)
        {
                std::cerr << "Failed to get surface image formats." << std::endl;
                return false;
        }

        if (std::find(imageFormats.begin(), imageFormats.end(), (vk::SurfaceFormatKHR){vk::Format::eB8G8R8A8Unorm, vk::ColorSpaceKHR::eSrgbNonlinear}) == imageFormats.end())
        {
                std::cerr << "Device does not support image format." << std::endl;
                for (size_t i = 0; i < imageFormats.size(); ++i)
                        std::cerr << "Format(" << i << "): " << static_cast<uint32_t>(imageFormats[i].format) << ", " << static_cast<uint32_t>(imageFormats[i].colorSpace) << std::endl;
                status = vk::Result::eErrorInitializationFailed;
                return false;
        }

        std::vector<vk::PresentModeKHR> presentModes;
        std::tie(status, presentModes) = m_physical.getSurfacePresentModesKHR(surface);
        if (status != vk::Result::eSuccess)
        {
                std::cerr << "Failed to get surface present modes." << std::endl;
                return false;
        }

        vk::PresentModeKHR presentMode = vk::PresentModeKHR::eFifo;
        if (std::find(presentModes.begin(), presentModes.end(), vk::PresentModeKHR::eFifo) == presentModes.end())
        {
                std::cerr << "Device does not support fifo presetn mode." << std::endl;
                status = vk::Result::eErrorInitializationFailed;
                return false;
        }

        vk::SwapchainCreateInfoKHR swapchainCreateInfo;
        swapchainCreateInfo.setSurface(surface);
        swapchainCreateInfo.setMinImageCount(2);
        swapchainCreateInfo.setImageFormat(vk::Format::eB8G8R8A8Unorm);
        swapchainCreateInfo.setImageColorSpace(vk::ColorSpaceKHR::eSrgbNonlinear);
        swapchainCreateInfo.setImageExtent(*imageSize = capabilities.maxImageExtent);
        swapchainCreateInfo.setImageArrayLayers(1);
        swapchainCreateInfo.setImageUsage(vk::ImageUsageFlagBits::eColorAttachment);
        swapchainCreateInfo.setImageSharingMode(vk::SharingMode::eExclusive);
        swapchainCreateInfo.setQueueFamilyIndexCount(1);
        swapchainCreateInfo.setPQueueFamilyIndices(&m_familyIndex);
        swapchainCreateInfo.setPreTransform(vk::SurfaceTransformFlagBitsKHR::eIdentity);
        swapchainCreateInfo.setCompositeAlpha(vk::CompositeAlphaFlagBitsKHR::eOpaque);
        swapchainCreateInfo.setPresentMode(presentMode);
        swapchainCreateInfo.setClipped(true);

        vk::SwapchainKHR swapchain;
        std::tie(status, swapchain) = m_logical.createSwapchainKHR(swapchainCreateInfo);
        if (status != vk::Result::eSuccess)
        {
                std::cerr << "Failed to create swapchain" << std::endl;
                return false;
        }

        m_swapchains.insert(m_swapchains.end(), swapchain);

        return true;
}

bool Render::Device::CreateSurfaceSemaphore()
{
        vk::SemaphoreCreateInfo createInfo;
        vk::Semaphore sem;
        std::tie(status, sem) = m_logical.createSemaphore(createInfo);
        if (status != vk::Result::eSuccess)
        {
                std::cerr << "Failed to create framebuffer." << std::endl;
                return false;
        }
        m_imageAvailableSemaphores.insert(m_imageAvailableSemaphores.end(), sem);
        return true;
}

bool Render::Device::CreateSampler()
{
        vk::SamplerCreateInfo samplerInfo;
        samplerInfo.setMagFilter(vk::Filter::eNearest);
        samplerInfo.setMinFilter(vk::Filter::eNearest);
        samplerInfo.setAddressModeU(vk::SamplerAddressMode::eClampToEdge);
        samplerInfo.setAddressModeV(vk::SamplerAddressMode::eClampToEdge);
        samplerInfo.setAddressModeW(vk::SamplerAddressMode::eClampToEdge);
        samplerInfo.setBorderColor(vk::BorderColor::eIntOpaqueBlack);
        samplerInfo.setCompareOp(vk::CompareOp::eAlways);
        samplerInfo.setMipmapMode(vk::SamplerMipmapMode::eNearest);

        std::tie(status, m_sampler) = m_logical.createSampler(samplerInfo);
        if (status != vk::Result::eSuccess)
        {
                std::cerr << "Failed to create framebuffer." << std::endl;
                return false;
        }

        return true;
}

bool Render::Device::CreateShaders()
{
        m_vertexShader = new Shader();
        m_fragmentShader = new Shader();
        return m_vertexShader->Init(*this, "main_vertex",
                                    R"(#version 450

layout(location = 0) in vec4 inPosition;
layout(location = 1) in vec2 inUV;

layout(set = 1, binding = 0) uniform SUBO {
  mat4 viewProj;
} display;

layout(set = 0, binding = 0) uniform UBO {
  mat4 model;
} surface;

layout(location = 0) out vec2 outUV;

void main()
{
    outUV = inUV;
    gl_Position = display.viewProj * surface.model * inPosition;
    /* Full screen */
    //gl_Position = inPosition;
}
)",
                                    vk::ShaderStageFlagBits::eVertex) &&
               m_fragmentShader->Init(*this, "main_fragment",
                                      R"(#version 450

layout(location = 0) in vec2 inUV;

layout(set = 0, binding = 1) uniform sampler2D surface;

layout(location = 0) out vec4 outColor;

void main()
{
    outColor = texture(surface, inUV);
}
)",
                                      vk::ShaderStageFlagBits::eFragment);
}

bool Render::Device::CreateVMA()
{
        VmaAllocatorCreateInfo allocatorInfo = {};
        allocatorInfo.physicalDevice = m_physical;
        allocatorInfo.device = m_logical;
        status = vk::Result(vmaCreateAllocator(&allocatorInfo, &m_allocator));
        if (status != vk::Result::eSuccess)
        {
                std::cerr << "Failed to create allocator." << std::endl;
                return false;
        }

        return true;
}

bool Render::Device::CreateDescriptorPool()
{
        std::vector<vk::DescriptorPoolSize> poolSizes = {
            {vk::DescriptorType::eSampler, 4096},
            {vk::DescriptorType::eCombinedImageSampler, 4096},
            {vk::DescriptorType::eSampledImage, 4096},
            {vk::DescriptorType::eStorageImage, 4096},
            {vk::DescriptorType::eUniformTexelBuffer, 4096},
            {vk::DescriptorType::eStorageTexelBuffer, 4096},
            {vk::DescriptorType::eUniformBuffer, 4096},
            {vk::DescriptorType::eStorageBuffer, 4096},
            {vk::DescriptorType::eUniformBufferDynamic, 4096},
            {vk::DescriptorType::eStorageBufferDynamic, 4096},
            {vk::DescriptorType::eInputAttachment, 4096}};

        vk::DescriptorPoolCreateInfo poolInfo;
        poolInfo.setPoolSizeCount(poolSizes.size());
        poolInfo.setPPoolSizes(poolSizes.data());
        poolInfo.setMaxSets(12 * 4096);
        poolInfo.setFlags(vk::DescriptorPoolCreateFlagBits::eFreeDescriptorSet);

        std::tie(status, m_descriptorPool) = m_logical.createDescriptorPool(poolInfo);
        if (status != vk::Result::eSuccess)
        {
                std::cerr << "Failed to create descriptor pool." << std::endl;
                return false;
        }

        return true;
}

bool Render::Device::CreateDescriptorLayout()
{
        std::vector<vk::DescriptorSetLayoutBinding> layoutBindings;
        layoutBindings.insert(layoutBindings.end(),
                              vk::DescriptorSetLayoutBinding(0, vk::DescriptorType::eUniformBuffer, 1, vk::ShaderStageFlagBits::eVertex));
        layoutBindings.insert(layoutBindings.end(),
                              vk::DescriptorSetLayoutBinding(1, vk::DescriptorType::eCombinedImageSampler, 1, vk::ShaderStageFlagBits::eFragment));

        vk::DescriptorSetLayoutCreateInfo layoutInfo;
        layoutInfo.setBindingCount(layoutBindings.size());
        layoutInfo.setPBindings(layoutBindings.data());

        std::tie(status, m_descriptorLayouts[0]) = m_logical.createDescriptorSetLayout(layoutInfo);
        if (status != vk::Result::eSuccess)
        {
                std::cerr << "Failed to create descriptor layout." << std::endl;
                return false;
        }

        layoutBindings = {};
        layoutBindings.insert(layoutBindings.end(),
                              vk::DescriptorSetLayoutBinding(0, vk::DescriptorType::eUniformBuffer, 1, vk::ShaderStageFlagBits::eVertex));

        layoutInfo.setBindingCount(layoutBindings.size());
        layoutInfo.setPBindings(layoutBindings.data());

        std::tie(status, m_descriptorLayouts[1]) = m_logical.createDescriptorSetLayout(layoutInfo);
        if (status != vk::Result::eSuccess)
        {
                std::cerr << "Failed to create descriptor layout." << std::endl;
                return false;
        }

        return true;
}

bool Render::Device::CreatePipeline()
{
        vk::PipelineLayoutCreateInfo layoutCreateInfo;
        layoutCreateInfo.setSetLayoutCount(2);
        layoutCreateInfo.setPSetLayouts(m_descriptorLayouts);

        std::tie(status, m_pipelineLayout) = m_logical.createPipelineLayout(layoutCreateInfo);
        if (status != vk::Result::eSuccess)
        {
                std::cerr << "Failed to create pipeline layout." << std::endl;
                return false;
        }

        vk::PipelineCacheCreateInfo cacheCreateInfo;
        std::tie(status, m_pipelineCache) = m_logical.createPipelineCache(cacheCreateInfo);
        if (status != vk::Result::eSuccess)
        {
                std::cerr << "Failed to create pipeline cache." << std::endl;
                return false;
        }

        return true;
}

bool Render::Device::CreateVertexBuffer()
{
        m_vertexBuffer = new Buffer();
        return m_vertexBuffer->Stage(*this, s_vertices, sizeof(s_vertices), vk::BufferUsageFlagBits::eVertexBuffer);
}

bool Render::Device::CreateCommandPool()
{
        vk::CommandPoolCreateInfo cmdPoolCreateInfo;
        cmdPoolCreateInfo.setFlags(vk::CommandPoolCreateFlagBits::eResetCommandBuffer);
        cmdPoolCreateInfo.setQueueFamilyIndex(m_familyIndex);

        std::tie(status, m_commandPool) = m_logical.createCommandPool(cmdPoolCreateInfo);
        if (status != vk::Result::eSuccess)
        {
                std::cerr << "Failed to create command pool." << std::endl;
                return false;
        }

        return true;
}

bool Render::Device::CreatePrimaryBuffer()
{
        vk::CommandBufferAllocateInfo cmdAllocInfo;
        cmdAllocInfo.setCommandBufferCount(1);
        cmdAllocInfo.setCommandPool(m_commandPool);
        cmdAllocInfo.setLevel(vk::CommandBufferLevel::ePrimary);

        std::vector<vk::CommandBuffer> storage;
        std::tie(status, storage) = m_logical.allocateCommandBuffers(cmdAllocInfo);
        if (status != vk::Result::eSuccess)
        {
                std::cerr << "Failed to create primary command buffer." << std::endl;
                return false;
        }
        m_commandBuffer = storage.front();

        return true;
}

void Render::Device::SurfaceCommit(Wayland_Compositor_Surface *surface, bool changed,
                                   void *data,
                                   int32_t stride,
                                   vk::Format format,
                                   int32_t width,
                                   int32_t height,
                                   int32_t sx,
                                   int32_t sy,
                                   bool newly_attached,
                                   damage_list damage_surface,
                                   damage_list damage_buffer,
                                   region_changelist opaque,
                                   region_changelist input,
                                   int transform,
                                   int32_t scale)
{
        m_buffers[surface]->Commit(
            changed,
            data,
            stride,
            format,
            width,
            height,
            sx,
            sy,
            newly_attached,
            damage_surface,
            damage_buffer,
            opaque,
            input,
            transform,
            scale);
}

void Render::Device::SurfaceCommit(Wayland_Compositor_Surface *surface,
                                   bool changed,
                                   int32_t width,
                                   int32_t height,
                                   std::pair<vk::Format, bool> format,
                                   uint32_t flags,
                                   int n_planes,
                                   int *fd,
                                   uint32_t *offset,
                                   uint32_t *stride,
                                   uint64_t *modifier,
                                   int32_t sx,
                                   int32_t sy,
                                   bool newly_attached,
                                   damage_list damage_surface,
                                   damage_list damage_buffer,
                                   region_changelist opaque,
                                   region_changelist input,
                                   int transform,
                                   int32_t scale)
{
        m_buffers[surface]->Commit(
            changed,
            width,
            height,
            format,
            flags,
            n_planes,
            fd,
            offset,
            stride,
            modifier,
            sx,
            sy,
            newly_attached,
            damage_surface,
            damage_buffer,
            opaque,
            input,
            transform,
            scale);
}

void Render::Device::InsertSurface(Wayland_Compositor_Surface *surface)
{
        m_buffers.insert(std::make_pair(surface, new Surface()));

        if (!m_buffers[surface]->Init(this))
                m_buffers.erase(surface);
}

void Render::Device::RemoveSurface(Wayland_Compositor_Surface *surface)
{
        m_buffers.erase(surface);
}

Render::Device::~Device() { Shutdown(); }

} // namespace vkc