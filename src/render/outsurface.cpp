/*
 * Copyright (C) 2018 by Ilya Glushchenko
 * Copyright (C) 2018 by Michael Mestnik
 * This code is licensed under the MIT license (MIT)
 * (http://opensource.org/licenses/MIT)
 */
#include "outsurface.hpp"
#include "surface.hpp"
#include "structs.hpp"
#include <iostream>
#include <unistd.h>

#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtx/string_cast.hpp>

namespace vkc
{

bool Render::Device::OutSurface::Init(Device *_device, vk::SurfaceKHR surface, vk::SwapchainKHR swapchain, vk::Extent2D imageSize, vk::Semaphore imageAvailableSemaphore)
{
        device = _device;
        m_surface = surface;
        m_swapchain = swapchain;
        m_imageSize = imageSize;
        m_imageAvailableSemaphore = imageAvailableSemaphore;
        return CreateRenderPass() &&
               CreateSwapchainImageViews() &&
               CreateDescriptorSet() &&
               CreateUniformBuffer() &&
               CreatePipeline() &&
               true;
}

void Render::Device::OutSurface::Shutdown()
{
        auto m_logical = device->m_logical;

        if (m_descriptorSet)
        {
                status = device->m_logical.freeDescriptorSets(device->m_descriptorPool, m_descriptorSet);
                if (status != vk::Result::eSuccess)
                {
                        std::cerr << "Warning: Failed to free descriptor set." << std::endl;
                }
        }

        if (m_imageAvailableSemaphore)
                m_logical.destroySemaphore(m_imageAvailableSemaphore);
        if (m_renderPass)
                m_logical.destroyRenderPass(m_renderPass);
        if (m_pipeline)
                m_logical.destroyPipeline(m_pipeline);
        for (auto it : m_nextImages)
        {
                if (it.m_framebuffer)
                        m_logical.destroyFramebuffer(it.m_framebuffer);
                if (it.m_imageView)
                        m_logical.destroyImageView(it.m_imageView);
        }
        if (m_uniformBuffer && m_uniformAllocation)
                vmaDestroyBuffer(device->m_allocator, m_uniformBuffer, m_uniformAllocation);
        if (m_swapchain)
                m_logical.destroySwapchainKHR(m_swapchain);
}

bool Render::Device::OutSurface::Frame()
{
        auto m_logical = device->m_logical;
        auto m_commandBuffer = device->m_commandBuffer;
        auto m_vertexBuffer = device->m_vertexBuffer->buffer;
        std::tie(status, m_currentFrameBuffer) = m_logical.acquireNextImageKHR(
            m_swapchain, UINT64_MAX, m_imageAvailableSemaphore, nullptr);
        if (status != vk::Result::eSuccess)
        {
                std::cerr << "Failed to acquire framebuffer image." << std::endl;
                return false;
        }

        std::vector<vk::ClearValue> clearValues;
        clearValues.resize(2);
        clearValues[0].setDepthStencil(vk::ClearDepthStencilValue(1.f, 0));
        clearValues[1].setColor(vk::ClearColorValue());

        vk::RenderPassBeginInfo renderPassBegin;
        renderPassBegin.setFramebuffer(m_nextImages[m_currentFrameBuffer].m_framebuffer);
        renderPassBegin.setRenderArea(vk::Rect2D({0, 0}, m_imageSize));
        renderPassBegin.setRenderPass(m_renderPass);
        renderPassBegin.setClearValueCount(clearValues.size());
        renderPassBegin.setPClearValues(clearValues.data());

        m_commandBuffer.beginRenderPass(renderPassBegin, vk::SubpassContents::eInline);
        m_commandBuffer.bindPipeline(vk::PipelineBindPoint::eGraphics, m_pipeline);
        {
                vk::DeviceSize offsets[] = {0};
                m_commandBuffer.bindVertexBuffers(0, 1, &m_vertexBuffer, offsets);
        }

        m_commandBuffer.bindDescriptorSets(
            vk::PipelineBindPoint::eGraphics,
            device->m_pipelineLayout, 1,
            m_descriptorSet, nullptr);

        for (auto it : device->m_buffers)
        {
                if (it.second->m_descriptorSet)
                {
                        m_commandBuffer.bindDescriptorSets(
                            vk::PipelineBindPoint::eGraphics,
                            device->m_pipelineLayout, 0,
                            it.second->m_descriptorSet, nullptr);
                        m_commandBuffer.draw(6, 1, 0, 0);
                }
        }

        m_commandBuffer.endRenderPass();

        return true;
}

bool Render::Device::OutSurface::CreatePipeline()
{
        auto m_logical = device->m_logical;
        auto m_vertexShader = device->m_vertexShader->shaderStage;
        auto m_fragmentShader = device->m_fragmentShader->shaderStage;
        auto m_pipelineLayout = device->m_pipelineLayout;
        auto m_pipelineCache = device->m_pipelineCache;

        vk::PipelineVertexInputStateCreateInfo vertexInputCreateInfo;
        vertexInputCreateInfo.setVertexAttributeDescriptionCount(2);
        vertexInputCreateInfo.setPVertexAttributeDescriptions(Vertex::s_inputAttributeDescription);
        vertexInputCreateInfo.setVertexBindingDescriptionCount(1);
        vertexInputCreateInfo.setPVertexBindingDescriptions(&Vertex::s_inputBindingDescription);

        vk::PipelineInputAssemblyStateCreateInfo inputAssemblyCreateInfo;
        inputAssemblyCreateInfo.setTopology(vk::PrimitiveTopology::eTriangleList);
        inputAssemblyCreateInfo.setPrimitiveRestartEnable(false);

        vk::PipelineShaderStageCreateInfo const shaderStages[] = {
            m_vertexShader, m_fragmentShader};

        vk::Viewport viewport;
        viewport.setWidth(m_imageSize.width);
        viewport.setHeight(m_imageSize.height);
        viewport.setX(0);
        viewport.setY(0);
        viewport.setMinDepth(0);
        viewport.setMaxDepth(1.f);

        vk::Rect2D scissor;
        scissor.setOffset({0, 0});
        scissor.setExtent(m_imageSize);

        vk::PipelineViewportStateCreateInfo viewportCreateInfo;
        viewportCreateInfo.setScissorCount(1);
        viewportCreateInfo.setPScissors(&scissor);
        viewportCreateInfo.setViewportCount(1);
        viewportCreateInfo.setPViewports(&viewport);

        vk::PipelineRasterizationStateCreateInfo rasterizationCreateInfo;
        rasterizationCreateInfo.setCullMode(vk::CullModeFlagBits::eNone);
        rasterizationCreateInfo.setPolygonMode(vk::PolygonMode::eFill);
        rasterizationCreateInfo.setFrontFace(vk::FrontFace::eClockwise);
        rasterizationCreateInfo.setLineWidth(1.f);
        rasterizationCreateInfo.setDepthClampEnable(false);
        rasterizationCreateInfo.setDepthBiasEnable(false);
        rasterizationCreateInfo.setRasterizerDiscardEnable(false);

        vk::PipelineMultisampleStateCreateInfo multisamplingCreateInfo;
        multisamplingCreateInfo.setSampleShadingEnable(false);
        multisamplingCreateInfo.setRasterizationSamples(vk::SampleCountFlagBits::e1);

        vk::PipelineDepthStencilStateCreateInfo depthStencilStateCreateInfo;
        depthStencilStateCreateInfo.setDepthTestEnable(true);
        depthStencilStateCreateInfo.setDepthWriteEnable(true);
        depthStencilStateCreateInfo.setDepthCompareOp(vk::CompareOp::eLess);
        depthStencilStateCreateInfo.setDepthBoundsTestEnable(false);
        depthStencilStateCreateInfo.setStencilTestEnable(false);

        vk::PipelineColorBlendAttachmentState colorBlendAttachmentState;
        colorBlendAttachmentState.setBlendEnable(true);
        colorBlendAttachmentState.setSrcColorBlendFactor(vk::BlendFactor::eSrc1Alpha);
        colorBlendAttachmentState.setDstColorBlendFactor(vk::BlendFactor::eOneMinusSrcAlpha);
        colorBlendAttachmentState.setColorBlendOp(vk::BlendOp::eAdd);
        colorBlendAttachmentState.setSrcAlphaBlendFactor(vk::BlendFactor::eOne);
        colorBlendAttachmentState.setDstAlphaBlendFactor(vk::BlendFactor::eZero);
        colorBlendAttachmentState.setAlphaBlendOp(vk::BlendOp::eAdd);
        colorBlendAttachmentState.setColorWriteMask(
            vk::ColorComponentFlagBits::eR | vk::ColorComponentFlagBits::eG | vk::ColorComponentFlagBits::eB | vk::ColorComponentFlagBits::eA);

        vk::PipelineColorBlendStateCreateInfo colorBlendCreateInfo;
        colorBlendCreateInfo.setAttachmentCount(1);
        colorBlendCreateInfo.setPAttachments(&colorBlendAttachmentState);
        colorBlendCreateInfo.setLogicOpEnable(false);

        vk::GraphicsPipelineCreateInfo pipelineCreateInfo;
        pipelineCreateInfo.setPVertexInputState(&vertexInputCreateInfo);
        pipelineCreateInfo.setPInputAssemblyState(&inputAssemblyCreateInfo);
        pipelineCreateInfo.setStageCount(2);
        pipelineCreateInfo.setPStages(shaderStages);
        pipelineCreateInfo.setPViewportState(&viewportCreateInfo);
        pipelineCreateInfo.setPRasterizationState(&rasterizationCreateInfo);
        pipelineCreateInfo.setPMultisampleState(&multisamplingCreateInfo);
        pipelineCreateInfo.setPDepthStencilState(&depthStencilStateCreateInfo);
        pipelineCreateInfo.setPColorBlendState(&colorBlendCreateInfo);
        pipelineCreateInfo.setRenderPass(m_renderPass);
        pipelineCreateInfo.setSubpass(0);
        pipelineCreateInfo.setLayout(m_pipelineLayout);

        std::tie(status, m_pipeline) = m_logical.createGraphicsPipeline(m_pipelineCache, pipelineCreateInfo);
        if (status != vk::Result::eSuccess)
        {
                std::cerr << "Failed to create graphics pipeline." << std::endl;
                return false;
        }

        return true;
}

bool Render::Device::OutSurface::CreateDescriptorSet()
{
        vk::DescriptorSetAllocateInfo allocInfo;
        allocInfo.setDescriptorPool(device->m_descriptorPool);
        allocInfo.setDescriptorSetCount(1);
        allocInfo.setPSetLayouts(&device->m_descriptorLayouts[1]);

        std::vector<vk::DescriptorSet> sets;
        std::tie(status, sets) = device->m_logical.allocateDescriptorSets(allocInfo);
        if (status != vk::Result::eSuccess)
        {
                std::cerr << "Failed to create render done semaphore." << std::endl;
                return false;
        }

        m_descriptorSet = sets[0];

        return true;
}

bool Render::Device::OutSurface::CreateUniformBuffer()
{
        vk::BufferCreateInfo bufferInfo;
        bufferInfo.setSize(sizeof(glm::mat4));
        bufferInfo.setUsage(vk::BufferUsageFlagBits::eUniformBuffer);

        VkBufferCreateInfo tmpInfo;
        tmpInfo = bufferInfo;

        VmaAllocationCreateInfo allocCreateInfo = {};
        allocCreateInfo.usage = VMA_MEMORY_USAGE_CPU_TO_GPU;
        allocCreateInfo.requiredFlags = VK_MEMORY_PROPERTY_HOST_COHERENT_BIT;

        VkBuffer tmpBuffer;
        status = vk::Result(vmaCreateBuffer(device->m_allocator, &tmpInfo, &allocCreateInfo, &tmpBuffer, &m_uniformAllocation, nullptr));
        if (status != vk::Result::eSuccess)
        {
                std::cerr << "Failed to create uniform buffer." << std::endl;
                return false;
        }

        m_uniformBuffer = tmpBuffer;

        auto projMat = glm::ortho(0.f, (float)m_imageSize.width, 0.f, (float)m_imageSize.height, 2.f, 0.f);
        auto viewMat = glm::lookAt(glm::vec3((float)m_imageSize.width / 2.f, (float)m_imageSize.height / 2.f, 1.f),
                                   glm::vec3((float)m_imageSize.width / 2.f, (float)m_imageSize.height / 2.f, 0.f),
                                   glm::vec3(0.f, 1.f, 0.f));

        glm::mat4 *uniform;
        vmaMapMemory(device->m_allocator, m_uniformAllocation, reinterpret_cast<void **>(&uniform));
        *uniform = projMat * viewMat;
        vmaUnmapMemory(device->m_allocator, m_uniformAllocation);

        vk::DescriptorBufferInfo descriptorInfo;
        descriptorInfo.setBuffer(m_uniformBuffer);
        descriptorInfo.setRange(sizeof(glm::mat4));

        vk::WriteDescriptorSet writeInfo;
        writeInfo.setDescriptorType(vk::DescriptorType::eUniformBuffer);
        writeInfo.setDescriptorCount(1);
        writeInfo.setPBufferInfo(&descriptorInfo);
        writeInfo.setDstSet(m_descriptorSet);
        device->m_logical.updateDescriptorSets(writeInfo, nullptr);

        return true;
}

bool Render::Device::OutSurface::CreateRenderPass()
{
        auto m_logical = device->m_logical;

        std::vector<vk::AttachmentDescription> attachmentDescriptions;
        attachmentDescriptions.resize(2);
        attachmentDescriptions[0].setFormat(vk::Format::eD32SfloatS8Uint);
        attachmentDescriptions[0].setSamples(vk::SampleCountFlagBits::e1);
        attachmentDescriptions[0].setInitialLayout(vk::ImageLayout::eUndefined);
        attachmentDescriptions[0].setFinalLayout(vk::ImageLayout::eDepthStencilAttachmentOptimal);
        attachmentDescriptions[0].setLoadOp(vk::AttachmentLoadOp::eClear);
        attachmentDescriptions[0].setStoreOp(vk::AttachmentStoreOp::eDontCare);
        attachmentDescriptions[0].setStencilLoadOp(vk::AttachmentLoadOp::eClear);
        attachmentDescriptions[0].setStencilStoreOp(vk::AttachmentStoreOp::eDontCare);
        attachmentDescriptions[1].setFormat(vk::Format::eB8G8R8A8Unorm);
        attachmentDescriptions[1].setSamples(vk::SampleCountFlagBits::e1);
        attachmentDescriptions[1].setInitialLayout(vk::ImageLayout::eUndefined);
        attachmentDescriptions[1].setFinalLayout(vk::ImageLayout::ePresentSrcKHR);
        attachmentDescriptions[1].setLoadOp(vk::AttachmentLoadOp::eClear);
        attachmentDescriptions[1].setStoreOp(vk::AttachmentStoreOp::eStore);
        attachmentDescriptions[1].setStencilLoadOp(vk::AttachmentLoadOp::eClear);
        attachmentDescriptions[1].setStencilStoreOp(vk::AttachmentStoreOp::eDontCare);

        vk::AttachmentReference attachmentDepth;
        attachmentDepth.setLayout(vk::ImageLayout::eDepthStencilAttachmentOptimal);

        vk::AttachmentReference attachmentColor;
        attachmentColor.setAttachment(1);
        attachmentColor.setLayout(vk::ImageLayout::eColorAttachmentOptimal);

        vk::SubpassDescription subpass;
        subpass.setColorAttachmentCount(1);
        subpass.setPColorAttachments(&attachmentColor);
        subpass.setPDepthStencilAttachment(&attachmentDepth);
        subpass.setPipelineBindPoint(vk::PipelineBindPoint::eGraphics);

        vk::RenderPassCreateInfo renderPassCreateInfo;
        renderPassCreateInfo.setAttachmentCount(attachmentDescriptions.size());
        renderPassCreateInfo.setPAttachments(attachmentDescriptions.data());
        renderPassCreateInfo.setSubpassCount(1);
        renderPassCreateInfo.setPSubpasses(&subpass);

        std::tie(status, m_renderPass) = m_logical.createRenderPass(renderPassCreateInfo);
        if (status != vk::Result::eSuccess)
        {
                std::cerr << "Failed to create renderpass." << std::endl;
                return false;
        }

        return true;
}

bool Render::Device::OutSurface::CreateSwapchainImageViews()
{
        auto m_logical = device->m_logical;
        std::vector<vk::Image> images;
        std::tie(status, images) = m_logical.getSwapchainImagesKHR(m_swapchain);
        if (status != vk::Result::eSuccess)
        {
                std::cerr << "Failed to get swapchain images." << std::endl;
                return false;
        }

        m_nextImages.resize(images.size());
        for (size_t i = 0; i < images.size(); ++i)
        {
                m_nextImages[i].m_image = images[i];

                vk::ImageSubresourceRange range;
                range.setLayerCount(1);
                range.setLevelCount(1);
                range.setAspectMask(vk::ImageAspectFlagBits::eColor);

                vk::ImageViewCreateInfo imageViewCreateInfo;
                imageViewCreateInfo.setFormat(vk::Format::eB8G8R8A8Unorm);
                imageViewCreateInfo.setImage(images[i]);
                imageViewCreateInfo.setViewType(vk::ImageViewType::e2D);
                imageViewCreateInfo.setSubresourceRange(range);
                imageViewCreateInfo.setComponents({vk::ComponentSwizzle::eR, vk::ComponentSwizzle::eG, vk::ComponentSwizzle::eB, vk::ComponentSwizzle::eA});

                std::tie(status, m_nextImages[i].m_imageView) = m_logical.createImageView(imageViewCreateInfo);
                if (status != vk::Result::eSuccess)
                {
                        std::cerr << "Failed to create swapchain image view." << std::endl;
                        return false;
                }

                vk::ImageCreateInfo imageInfo;
                imageInfo.setImageType(vk::ImageType::e2D);
                imageInfo.setFormat(vk::Format::eD32SfloatS8Uint);
                imageInfo.setExtent(vk::Extent3D(m_imageSize.width, m_imageSize.height, 1));
                imageInfo.setMipLevels(1);
                imageInfo.setArrayLayers(1);
                imageInfo.setSamples(vk::SampleCountFlagBits::e1);
                imageInfo.setUsage(vk::ImageUsageFlagBits::eDepthStencilAttachment);

                VkImageCreateInfo tmpImageInfo;
                tmpImageInfo = imageInfo;

                VmaAllocationCreateInfo allocInfo = {};
                allocInfo.usage = VMA_MEMORY_USAGE_GPU_ONLY;

                VkImage imageTemp;
                status = vk::Result(vmaCreateImage(
                    device->m_allocator,
                    &tmpImageInfo, &allocInfo, &imageTemp,
                    &m_nextImages[i].m_depthStencilAllocation, nullptr));
                if (status != vk::Result::eSuccess)
                {
                        std::cerr << "failed to create image" << std::endl;
                        return false;
                }

                m_nextImages[i].m_depthStencil = imageTemp;

                vk::ImageViewCreateInfo viewInfo;
                viewInfo.setImage(m_nextImages[i].m_depthStencil);
                viewInfo.setViewType(vk::ImageViewType::e2D);
                viewInfo.setFormat(vk::Format::eD32SfloatS8Uint);
                viewInfo.setSubresourceRange(vk::ImageSubresourceRange(vk::ImageAspectFlagBits::eDepth | vk::ImageAspectFlagBits::eStencil, 0, 1, 0, 1));

                std::tie(status, m_nextImages[i].m_depthStencilView) = device->m_logical.createImageView(viewInfo);
                if (status != vk::Result::eSuccess)
                {
                        std::cerr << "failed to allocate texture view" << std::endl;
                        return false;
                }

                std::vector<vk::ImageView> attachments({m_nextImages[i].m_depthStencilView, m_nextImages[i].m_imageView});
                vk::FramebufferCreateInfo framebufferCreateInfo;
                framebufferCreateInfo.setRenderPass(m_renderPass);
                framebufferCreateInfo.setAttachmentCount(attachments.size());
                framebufferCreateInfo.setPAttachments(attachments.data());
                framebufferCreateInfo.setWidth(m_imageSize.width);
                framebufferCreateInfo.setHeight(m_imageSize.height);
                framebufferCreateInfo.setLayers(1);

                std::tie(status, m_nextImages[i].m_framebuffer) = m_logical.createFramebuffer(framebufferCreateInfo);
                if (status != vk::Result::eSuccess)
                {
                        std::cerr << "Failed to create framebuffer." << std::endl;
                        return false;
                }
        }

        return true;
}

} // namespace vkc