/*
 * Copyright (C) 2018 by Ilya Glushchenko
 * This code is licensed under the MIT license (MIT)
 * (http://opensource.org/licenses/MIT)
 */
#include "helpers.hpp"
#include "structs.hpp"

namespace vkc
{

vk::VertexInputBindingDescription const Render::Device::Vertex::s_inputBindingDescription = {
    0, sizeof(Vertex), vk::VertexInputRate::eVertex};
vk::VertexInputAttributeDescription const Render::Device::Vertex::s_inputAttributeDescription[] = {{
    0, 0, vk::Format::eR32G32B32A32Sfloat, offsetof(Vertex, pos)}, {
    1, 0, vk::Format::eR32G32Sfloat, offsetof(Vertex, uv)}};

bool Render::Device::Buffer::Stage(Render::Device &device, void const *data, size_t size, vk::BufferUsageFlagBits usage)
{
        return CreateBuffer(device, size, usage) && CopyMemory(device, data, size);
}

bool Render::Device::Buffer::CreateBuffer(Render::Device &device, size_t size, vk::BufferUsageFlagBits usage)
{
        vk::BufferCreateInfo bufferCreateInfo;
        bufferCreateInfo.setQueueFamilyIndexCount(1);
        bufferCreateInfo.setPQueueFamilyIndices(&device.m_familyIndex);
        bufferCreateInfo.setSharingMode(vk::SharingMode::eExclusive);
        bufferCreateInfo.setSize(size);
        bufferCreateInfo.setUsage(usage);

        VkBufferCreateInfo tmpInfo;
        tmpInfo = bufferCreateInfo;

        VmaAllocationCreateInfo bufferAllocInfo = {};
        bufferAllocInfo.usage = VMA_MEMORY_USAGE_CPU_TO_GPU;

        VkBuffer tmpBuffer;
        vk::Result status;
        status = vk::Result(vmaCreateBuffer(device.m_allocator, &tmpInfo, &bufferAllocInfo, &tmpBuffer, &allocation, nullptr));
        if (status != vk::Result::eSuccess)
        {
                std::cerr << "Failed to create buffer." << std::endl;
                return false;
        }

        buffer = tmpBuffer;

        return true;
}

bool Render::Device::Buffer::CopyMemory(Render::Device &device, void const *data, size_t size)
{
        void *mappedMemory;

        vk::Result status;
        status = vk::Result(vmaMapMemory(device.m_allocator, allocation, &mappedMemory));
        if (status != vk::Result::eSuccess)
        {
                std::cerr << "failed to map image memory!" << std::endl;
                return false;
        }
        memcpy(mappedMemory, data, size);
        vmaUnmapMemory(device.m_allocator, allocation);

        vmaFlushAllocation(device.m_allocator, allocation, 0, size);

        return true;
}

bool Render::Device::Shader::Init(Render::Device &device, const std::string name, const std::string _code, vk::ShaderStageFlagBits stage)
{
        std::vector<uint32_t> shaderCode;
        if (!CompileShader(name, _code, stage == vk::ShaderStageFlagBits::eVertex ? shaderc_vertex_shader : shaderc_fragment_shader, shaderCode))
        {
                std::cerr << "Failed to compile shader: " << name << std::endl;
                return false;
        }

        vk::ShaderModuleCreateInfo shaderModuleCreateInfo;
        shaderModuleCreateInfo.setCodeSize(static_cast<size_t>(shaderCode.size() * sizeof(int)));
        shaderModuleCreateInfo.setPCode(reinterpret_cast<uint32_t *>(shaderCode.data()));
        std::tie(state, shaderModule) = device.m_logical.createShaderModule(shaderModuleCreateInfo);
        if (state != vk::Result::eSuccess)
        {
                std::cerr << "Failed to create shader module: " << name << std::endl;
                return false;
        }

        shaderStage.setPName("main");
        shaderStage.setStage(stage);
        shaderStage.setModule(shaderModule);

        return true;
}

} // namespace vkc