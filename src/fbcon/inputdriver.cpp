/*
 * Copyright (C) 2018 by Michael Mestnik
 * This code is licensed under the MIT license (MIT)
 * (http://opensource.org/licenses/MIT)
 */
#include <iostream>
#include <fcntl.h>
#include <unistd.h>
#include <libudev.h>
#include <libinput.h>

#include "inputdriver.hpp"
#include "wayland.hpp"
#include "launcher-impl.h"

namespace vkc
{

namespace
{

const static struct libinput_interface interface = {
    .open_restricted = &launcher_logind_open,
    .close_restricted = &launcher_logind_close,
};

} // namespace

extern bool VulkanIO_running;

class InputDriver::impl
{
      public:
        bool Init(InputDriver *t)
        {
                m_this = t;

                if (launcher_logind_connect(3, "seat0", singleton_wayland->loop) != 0)
                {
                        std::cerr << "Failed launcher_logind_connect()" << std::endl;
                        return false;
                }
                if (!(m_udev = udev_new()))
                {
                        std::cerr << "Failed udev_new()" << std::endl;
                        return false;
                }
                if (!(m_li = libinput_udev_create_context(&interface, NULL, m_udev)))
                {
                        std::cerr << "Failed libinput_udev_create_context()" << std::endl;
                        return false;
                }
                if (libinput_udev_assign_seat(m_li, "seat0") != 0)
                {
                        std::cerr << "Failed libinput_udev_assign_seat()" << std::endl;
                        return false;
                }
                return true;
        }

        void PollEvents()
        {
                struct libinput_event *event;
                while (libinput_dispatch(m_li) == 0 &&
                       (event = libinput_get_event(m_li)) != NULL)
                {
                        static enum commandStates {
                                none,
                                down,
                                active,
                                clear
                        } commandState = commandStates::none;
                        static uint32_t last;

                        if (libinput_event_get_type(event) == libinput_event_type::LIBINPUT_EVENT_KEYBOARD_KEY)
                        {
                                auto key = libinput_event_get_keyboard_event(event);
                                if (!key)
                                        goto next_key;

                                switch (commandState)
                                {
                                case commandStates::clear:
                                        commandState = commandStates::none;
                                        if (libinput_event_keyboard_get_seat_key_count(key) == 0 &&
                                            libinput_event_keyboard_get_key_state(key) == libinput_key_state::LIBINPUT_KEY_STATE_RELEASED &&
                                            libinput_event_keyboard_get_key(key) == last)
                                                goto next_key;
                                        break;
                                case commandStates::active:
                                        if (libinput_event_keyboard_get_seat_key_count(key) == 1 &&
                                            libinput_event_keyboard_get_key_state(key) == libinput_key_state::LIBINPUT_KEY_STATE_PRESSED)
                                        {
                                                if ((last = libinput_event_keyboard_get_key(key)) == 0x002d)
                                                        VulkanIO_running = false;
                                                commandState = commandStates::clear;
                                                goto next_key;
                                        }
                                        else
                                        {
                                                commandState = commandStates::none;
                                                /* ToDo: Insert fake event */
                                        }
                                        break;
                                case commandStates::down:
                                        if (libinput_event_keyboard_get_seat_key_count(key) == 0 &&
                                            libinput_event_keyboard_get_key_state(key) == libinput_key_state::LIBINPUT_KEY_STATE_RELEASED &&
                                            libinput_event_keyboard_get_key(key) == 0x007d)
                                        {
                                                commandState = commandStates::active;
                                        }
                                        else
                                        {
                                                commandState = commandStates::none;
                                        }
                                        break;
                                case commandStates::none:
                                        if (libinput_event_keyboard_get_seat_key_count(key) == 1 &&
                                            libinput_event_keyboard_get_key_state(key) == libinput_key_state::LIBINPUT_KEY_STATE_PRESSED &&
                                            libinput_event_keyboard_get_key(key) == 0x007d)
                                        {
                                                commandState = commandStates::down;
                                        }
                                        break;
                                }
                        }

                next_key:
                        libinput_event_destroy(event);
                }
        }

        struct udev *m_udev;
        struct libinput *m_li;

      private:
        InputDriver *m_this;
};

InputDriver::InputDriver() : pImpl(std::make_unique<impl>()) {}
InputDriver::~InputDriver() { Shutdown(); }

bool InputDriver::Init() { return pImpl->Init(this); }

void InputDriver::Shutdown()
{
        libinput_unref(pImpl->m_li);
        udev_unref(pImpl->m_udev);
        launcher_logind_destroy();
}

void InputDriver::PollEvents()
{
        pImpl->PollEvents();
}

} // namespace vkc