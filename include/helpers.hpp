/*
 * Copyright (C) 2018 by Ilya Glushchenko
 * This code is licensed under the MIT license (MIT)
 * (http://opensource.org/licenses/MIT)
 */
#pragma once

#include "vulkan.hpp"
#include "shaderc/shaderc.hpp"

#include <tuple>
#include <iostream>
#include <cstdint>
#include <string>

namespace vkc
{

inline std::tuple<bool, uint32_t> FindMemoryTypeIndex(vk::PhysicalDevice device, vk::MemoryPropertyFlags flags)
{
        vk::PhysicalDeviceMemoryProperties const memoryProperties = device.getMemoryProperties();
        uint32_t memoryTypeIndex = 0;
        bool memoryTypeFound = false;

        for (uint32_t i = 0; i < memoryProperties.memoryTypeCount; ++i)
        {
                if (static_cast<bool>(memoryProperties.memoryTypes[i].propertyFlags & flags))
                {
                        memoryTypeIndex = i;
                        memoryTypeFound = true;
                        break;
                }
        }

        return std::make_tuple(memoryTypeFound, memoryTypeIndex);
}

namespace
{

// Helper function to check if the compilation result indicates a successful
// compilation.
template <typename T>
bool CompilationResultIsSuccess(const shaderc::CompilationResult<T> &result)
{
        return result.GetCompilationStatus() == shaderc_compilation_status_success;
}

} // namespace

inline bool CompileShader(const std::string name, const std::string _code, shaderc_shader_kind kind, std::vector<uint32_t> &spirv)
{
        shaderc::Compiler compiler;

        shaderc::SpvCompilationResult result = compiler.CompileGlslToSpv(_code.c_str(), _code.length(), kind, name.c_str());
        if (!CompilationResultIsSuccess(result))
        {
                std::cerr << "Failed shaderc(" << result.GetCompilationStatus() << "): " << result.GetErrorMessage() << std::endl;
                spirv.clear();

                std::cerr << "Failed shader source: " << _code << std::endl;
                return false;
        }

        spirv.assign(result.cbegin(), result.cend());

        return true;
}

} // namespace vkc
