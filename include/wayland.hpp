/*
 * Copyright (C) 2018 by Michael Mestnik
 * This code is licensed under the MIT license (MIT)
 * (http://opensource.org/licenses/MIT)
 */
#pragma once

#include <vector>
#include <wayland-server.h>

#include "render.hpp"

namespace vkc
{

#define MAX_DMABUF_PLANES 4096

class Wayland_Compositor_Surface
{
      public:
        wl_resource *resource;

        class active
        {
              public:
                bool changed;
                class buffer
                {
                      public:
                        buffer() = default;
                        buffer(buffer *b);

                        enum buffer_type
                        {
                                eSHM,
                                eDMABUF
                        } type = buffer_type::eSHM;
                        struct wl_resource *resource;
                        struct wl_resource *params_resource;
                        class dmabuf_attributes
                        {
                              public:
                                int32_t width;
                                int32_t height;
                                std::pair<vk::Format, bool> format;
                                uint32_t flags; /* enum zlinux_buffer_params_flags */
                                int n_planes;
                                int fd[MAX_DMABUF_PLANES];
                                uint32_t offset[MAX_DMABUF_PLANES];
                                uint32_t stride[MAX_DMABUF_PLANES];
                                uint64_t modifier[MAX_DMABUF_PLANES];
                        } attributes;

                        wl_listener destroy_listener;
                        wl_signal destroy_signal;
                } * m_buffer;
                int32_t sx;
                int32_t sy;
                bool newly_attached;
                wl_listener buffer_destroy_listener;
                damage_list m_damage_surface;
                damage_list m_damage_buffer;
                class frame
                {
                      public:
                        wl_resource *resource;
                };
                std::vector<frame *> m_frames;
                region_changelist opaque;
                region_changelist input;
                int transform;
                int32_t scale;
        } pending;
};

class Wayland
{
      public:
        bool Init();

        void Loop();

        bool GlobalCreate(const wl_interface *interface, int version, void *data,
                          wl_global_bind_func_t bind);

        class Compositor
        {
              public:
                bool Init();

                class Region
                {
                      public:
                        wl_resource *resource;

                        region_changelist m_additions;
                };
        } m_Compositor;

        class Shell
        {
              public:
                bool Init();
        } m_Shell;

        class DmaBuf
        {
              public:
                bool Init();
        } m_DmaBuf;

        class Output
        {
              public:
                bool Init();
        } m_Output;

        class data_device_manager
        {
              public:
                bool Init();
                class data_device
                {
                      public:
                        void *seat;
                        wl_resource *resource;
                };
        } m_data_device_manager;

        class Subcompositor
        {
              public:
                bool Init();
        } m_Subcompositor;

        class zxdg_shell_v6
        {
              public:
                bool Init();

                class Surface
                {
                      public:
                        Wayland_Compositor_Surface *surface;
                        struct wl_resource *resource;
                        class TopLevel
                        {
                              public:
                                struct wl_resource *resource;
                        };
                };
        } m_zxdg_shell_v6;

        class Seat
        {
              public:
                bool Init();
        } m_Seat;

        struct wl_event_loop *loop;

      private:
        struct wl_display *display;
        struct wl_event_source *signals[4];
};

extern Wayland *singleton_wayland;

} // namespace vkc
